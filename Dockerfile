FROM maven:3.8.4-openjdk-17-slim as maven

COPY pom.xml /pom.xml

RUN mvn dependency:go-offline

COPY src /src

RUN mvn package -DskipTests

FROM openjdk:17-jdk-slim

ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /wait-for-it.sh

RUN chmod +x /wait-for-it.sh

COPY --from=maven target/xchange-globalsearch-*.jar /app.jar

ENTRYPOINT ["/bin/bash", "-c"]
#ENTRYPOINT [\
#  "java",\
#  "-jar","/app.jar"\
#]