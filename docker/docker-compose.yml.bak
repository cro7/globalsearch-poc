version: '3.2'

services:
  elasticsearch:
    build:
      context: elasticsearch/
      args:
        ELASTIC_VERSION: ${ELASTIC_VERSION}
    volumes:
      - ./elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml:ro,z
      - elasticsearch:/usr/share/elasticsearch/data:z
    ports:
      - "9200:9200"
      - "9300:9300"
    environment:
      ES_JAVA_OPTS: -Xmx256m -Xms256m
      # Bootstrap password.
      # Used to initialize the keystore during the initial startup of
      # Elasticsearch. Ignored on subsequent runs.
      ELASTIC_PASSWORD: changeme
      # Use single node discovery in order to disable production mode and avoid bootstrap checks.
      # see: https://www.elastic.co/guide/en/elasticsearch/reference/7.17/bootstrap-checks.html
      discovery.type: single-node
    networks:
      - globalsearch

  logstash:
    build:
      context: logstash/
      args:
        ELASTIC_VERSION: ${ELASTIC_VERSION}
    volumes:
      - ./logstash/config/logstash.yml:/usr/share/logstash/config/logstash.yml:ro,Z
      - ./logstash/config/pipelines.yml:/usr/share/logstash/config/pipelines.yml:ro,Z
      - ./logstash/pipeline:/usr/share/logstash/pipeline:ro,Z
      - type: bind
        source: logstash/mysql-connector-java-8.0.28.jar
        target: /usr/share/logstash/logstash-core/lib/jars/mysql-connector-java.jar
    ports:
      - "5044:5044"
      - "5000:5000/tcp"
      - "5000:5000/udp"
      - "9600:9600"
    environment:
      LS_JAVA_OPTS: -Xmx768m -Xms768m
    env_file: .env
    networks:
      - globalsearch
    depends_on:
      - elasticsearch
      - rabbitmq
      - globalsearch

  kibana:
    build:
      context: kibana/
      args:
        ELASTIC_VERSION: ${ELASTIC_VERSION}
    volumes:
      - ./kibana/config/kibana.yml:/usr/share/kibana/config/kibana.yml:ro,Z
    ports:
      - "5601:5601"
    networks:
      - globalsearch
    depends_on:
      - elasticsearch
        
  rabbitmq:
    build:
      context: rabbitmq
    ports:
      - "5673:5672"
      - "15672:15672"
    networks:
      - globalsearch

  globalsearch:
    build:
      context: ../
    ports:
      - "8080:8080"
    command: ["/wait-for-it.sh elasticsearch:9200 --strict --timeout=180 -- java -jar /app.jar"]
    networks:
      - globalsearch
    depends_on:
      - rabbitmq

networks:
  globalsearch:
    driver: bridge

volumes:
  elasticsearch:
