input {

  jdbc {
    jdbc_driver_library => "/usr/share/logstash/logstash-core/lib/jars/mysql-connector-java.jar"
    jdbc_driver_class => "com.mysql.cj.jdbc.Driver"
    jdbc_connection_string => "${TRADING_CONNECTION_STRING}"
    jdbc_user => "${TRADING_USERNAME}"
    jdbc_password => "${TRADING_PASSWORD}"
    tracking_column => "last_modified_date_in_secs"
    tracking_column_type => "numeric"
    use_column_value => true
    schedule => "*/15 * * * * *"
    statement => "
    SELECT d.id  as id,
           d.price as price,
           d.quantity as quantity,
           d.location_unlocode as location_unlocode,
           d.equipment_type as equipment_type_name,
           d.unit_condition as condition_name,
           dps.company_id as seller_company_id,
           dpb.company_id as buyer_company_id,
           cr.container_number as container_number,
           r.reference_number as reference_number,
           UNIX_TIMESTAMP(GREATEST(IFNULL(d.last_modified_date, 1), IFNULL(r.last_modified_date,1), ifnull(cr.last_modified_date, 1))) as last_modified_date_in_secs

    FROM T_DEAL d
             LEFT JOIN T_RELEASE r on d.id = r.deal_id
             LEFT JOIN T_CONTAINER_RELEASE cr on r.id = cr.release_id
             LEFT JOIN T_DEAL_PROPOSAL dpb on dpb.id = d.buyer_deal_proposal_id
             LEFT JOIN T_DEAL_PROPOSAL dps on dps.id = d.seller_deal_proposal_id
    WHERE UNIX_TIMESTAMP(GREATEST(IFNULL(d.last_modified_date, 1), IFNULL(r.last_modified_date,1), ifnull(cr.last_modified_date, 1))) > :sql_last_value
    and UNIX_TIMESTAMP(GREATEST(IFNULL(d.last_modified_date, 1), IFNULL(r.last_modified_date,1), ifnull(cr.last_modified_date, 1))) < now()
    "
  }
}

filter {
 jdbc_streaming {
    jdbc_driver_class => "com.mysql.cj.jdbc.Driver"
    jdbc_connection_string => "${XCHANGE_CONNECTION_STRING}"
    jdbc_user => "${XCHANGE_USERNAME}"
    jdbc_password => "${XCHANGE_PASSWORD}"
    statement => "SELECT id, default_name as name, unlocode, region, country  FROM T_LOCATION WHERE unlocode = :location_unlocode"
    parameters => { "location_unlocode" => "location_unlocode"}
    target => "location_tmp"
  }
  jdbc_streaming {
    jdbc_driver_class => "com.mysql.cj.jdbc.Driver"
    jdbc_connection_string => "${XCHANGE_CONNECTION_STRING}"
    jdbc_user => "${XCHANGE_USERNAME}"
    jdbc_password => "${XCHANGE_PASSWORD}"
    statement => "SELECT id, name FROM T_CARRIER WHERE id = :seller_company_id"
    parameters => { "seller_company_id" => "seller_company_id"}
    target => "seller_company_name_tmp"
  }
  jdbc_streaming {
    jdbc_driver_class => "com.mysql.cj.jdbc.Driver"
    jdbc_connection_string => "${XCHANGE_CONNECTION_STRING}"
    jdbc_user => "${XCHANGE_USERNAME}"
    jdbc_password => "${XCHANGE_PASSWORD}"
    statement => "SELECT id, name FROM T_CARRIER WHERE id = :buyer_company_id"
    parameters => { "buyer_company_id" => "buyer_company_id"}
    target => "buyer_company_name_tmp"
  }
  jdbc_streaming {
    jdbc_driver_class => "com.mysql.cj.jdbc.Driver"
    jdbc_connection_string => "${XCHANGE_CONNECTION_STRING}"
    jdbc_user => "${XCHANGE_USERNAME}"
    jdbc_password => "${XCHANGE_PASSWORD}"
    statement => "SELECT name, short_name, long_name FROM T_EQUIPMENT_TYPE WHERE name = :equipment_type_name"
    parameters => { "equipment_type_name" => "equipment_type_name"}
    target => "equipment_type_tmp"
  }

  mutate {
    add_field => {
      "[location][id]" => "%{[location_tmp][0][id]}"
      "[location][unlocode]" => "%{[location_tmp][0][unlocode]}"
      "[location][name]" => "%{[location_tmp][0][name]}"
      "[location][region]" => "%{[location_tmp][0][region]}"
      "[location][country]" => "%{[location_tmp][0][country]}"
      "[equipment_type][name]" => "%{[equipment_type_tmp][0][name]}"
      "[equipment_type][short_name]" => "%{[equipment_type_tmp][0][short_name]}"
      "[equipment_type][long_name]" => "%{[equipment_type_tmp][0][long_name]}"
      "[seller_company][id]" => "%{[seller_company_name_tmp][0][id]}"
      "[seller_company][name]" => "%{[seller_company_name_tmp][0][name]}"
      "[buyer_company][id]" => "%{[buyer_company_name_tmp][0][id]}"
      "[buyer_company][name]" => "%{[buyer_company_name_tmp][0][name]}"
    }
  }

    mutate {
      convert => {
        "id" => "string"
        "[location][id]" => "string"
        "[seller_company][id]" => "string"
        "[buyer_company][id]" => "string"
      }
    }

  aggregate {
    task_id => "%{id}"
    code => "
      map['id'] = event.get('id')
      map['price'] = event.get('price')
      map['quantity'] = event.get('quantity')
      map['location'] = event.get('location')
      map['seller_company'] = event.get('seller_company')
      map['buyer_company'] = event.get('buyer_company')
      map['equipment_type'] = event.get('equipment_type')
      map['condition_name'] = event.get('condition_name')
      map['location'] = event.get('location')

      map['released_containers_tmp'] ||= []
      map['released_containers'] ||= []
      if (event.get('container_number') != nil)
        if !( map['released_containers_tmp'].include? event.get('container_number') )
          map['released_containers_tmp'] << event.get('container_number')
          map['released_containers'] << {
            'container_number' => event.get('container_number'),
            'release_reference' => event.get('reference_number')
          }
        end
      end

      event.cancel()
    "
    push_previous_map_as_event => true
    timeout => 5
  }
  mutate {
    remove_field => ["released_containers_tmp", "location_tmp", "seller_company_name_tmp", "buyer_company_name_tmp",
    "equipment_type_tmp", "@version"]
  }
}

output {
  rabbitmq {
    host => "${RABBIT_HOST}"
    port => "${RABBIT_PORT}"
    user => "${RABBIT_USERNAME}"
    password => "${RABBIT_PASSWORD}"
    exchange => ""
    exchange_type => "direct"
    ssl => ${RABBIT_SSL}
    vhost => "${RABBIT_VHOST}"
    key => "xchange.globalsearch.input"
    message_properties => {
      "content_type" => "application/json"
      "headers" => {
            "action" => "TRADING_DEAL"
      }
    }
  }
}
