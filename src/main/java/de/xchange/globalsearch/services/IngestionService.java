package de.xchange.globalsearch.services;

import de.xchange.globalsearch.documents.GlobalDocument;
import de.xchange.globalsearch.interfaces.amqp.dto.Company;
import de.xchange.globalsearch.interfaces.amqp.dto.LeasingRequest;
import de.xchange.globalsearch.interfaces.amqp.dto.TradingDeal;
import de.xchange.globalsearch.repository.GlobalRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class IngestionService {

    private final GlobalRepository globalRepository;

    public void upsertCompany(Company company) {
        globalRepository.save(GlobalDocument.from(company));
    }

    public void upsertLeasingRequest(LeasingRequest leasingRequest) {
        globalRepository.save(GlobalDocument.from(leasingRequest));
    }

    public void upsertTradingDeal(TradingDeal tradingDeal) {
        globalRepository.save(GlobalDocument.from(tradingDeal));
    }

    public void upsertCompanys(List<Company> companies) {
        globalRepository.saveAll(companies.stream().map(GlobalDocument::from).collect(Collectors.toList()));
    }

    public void upsertLeasingRequests(List<LeasingRequest> leasingRequests) {
        globalRepository.saveAll(leasingRequests.stream().map(GlobalDocument::from).collect(Collectors.toList()));
    }

    public void upsertTradingDeals(List<TradingDeal> tradingDeals) {
        globalRepository.saveAll(tradingDeals.stream().map(GlobalDocument::from).collect(Collectors.toList()));
    }

}
