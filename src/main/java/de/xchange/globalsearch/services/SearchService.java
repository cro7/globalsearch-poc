package de.xchange.globalsearch.services;

import de.xchange.globalsearch.documents.GlobalDocument;
import de.xchange.globalsearch.interfaces.rest.SearchQueryDTO;
import java.util.Arrays;
import lombok.RequiredArgsConstructor;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SearchService {

    private final ElasticsearchOperations searchOperations;

    public SearchHits<GlobalDocument> searchGlobal(SearchQueryDTO searchQueryDTO) {
        QueryBuilder queryBuilder = Arrays.stream(searchQueryDTO.getSearchString().split(" "))
            .map(word -> {
                    BoolQueryBuilder builder = QueryBuilders.boolQuery();
                    addSearchField(builder, "tradingDeal.id", word);
                    addSearchField(builder, "tradingDeal.location.unlocode", word);
                    addSearchField(builder, "tradingDeal.location.name", word);
                    addSearchField(builder, "tradingDeal.location.region", word);
                    addSearchField(builder, "tradingDeal.location.country", word);
                    addSearchField(builder, "tradingDeal.quantity", word);
                    addSearchField(builder, "tradingDeal.equipmentType.shortName", word);
                    addSearchField(builder, "tradingDeal.equipmentType.longName", word);
                    addSearchField(builder, "tradingDeal.sellerCompany.name", word);
                    addSearchField(builder, "tradingDeal.buyerCompany.name", word);
                    addSearchField(builder, "tradingDeal.releasedContainers.referenceNumber", word);
                    addSearchField(builder, "tradingDeal.releasedContainers.containerNumber", word);
                    return builder;
                }
            )
            .collect(BoolQueryBuilder::new, BoolQueryBuilder::must, BoolQueryBuilder::must);
        NativeSearchQuery query = new NativeSearchQueryBuilder()
            .withQuery(queryBuilder)
            .withPageable(PageRequest.of(searchQueryDTO.getPageNum(), searchQueryDTO.getPageSize()))
            .build();
        return searchOperations.search(query, GlobalDocument.class);
    }

    private void addSearchField(BoolQueryBuilder boolQueryBuilder, String field, String word) {
        boolQueryBuilder
            .should(QueryBuilders.matchPhrasePrefixQuery(field, word))
            .should(QueryBuilders.fuzzyQuery(field, word).fuzziness(Fuzziness.AUTO));
    }

}
