package de.xchange.globalsearch.documents;

import de.xchange.globalsearch.interfaces.amqp.dto.Company;
import de.xchange.globalsearch.interfaces.amqp.dto.LeasingRequest;
import de.xchange.globalsearch.interfaces.amqp.dto.TradingDeal;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "global")
@Getter
@Setter
@ToString(callSuper = true)
public class GlobalDocument {

    @Id
    private String id;

    private GlobalType type;

    @Field(type = FieldType.Object)
    private CompanyDocument company;
    @Field(type = FieldType.Object)
    private LeasingRequestDocument leasingRequest;
    @Field(type = FieldType.Object)
    private TradingDealDocument tradingDeal;

    public enum GlobalType {
        COMPANY, LEASING_REQUEST, TRADING_DEAL
    }


    public static GlobalDocument from(TradingDeal tradingDeal) {
        GlobalDocument globalDocument = new GlobalDocument();
        globalDocument.setType(GlobalType.TRADING_DEAL);
        globalDocument.setId(GlobalType.TRADING_DEAL + tradingDeal.getId());
        globalDocument.setTradingDeal(TradingDealDocument.from(tradingDeal));
        return globalDocument;
    }

    public static GlobalDocument from(LeasingRequest leasingRequest) {
        GlobalDocument globalDocument = new GlobalDocument();
        globalDocument.setType(GlobalType.LEASING_REQUEST);
        globalDocument.setId(GlobalType.LEASING_REQUEST + leasingRequest.getId());
        globalDocument.setLeasingRequest(LeasingRequestDocument.from(leasingRequest));
        return globalDocument;
    }

    public static GlobalDocument from(Company company) {
        GlobalDocument globalDocument = new GlobalDocument();
        globalDocument.setType(GlobalType.COMPANY);
        globalDocument.setId(GlobalType.COMPANY + company.getId());
        globalDocument.setCompany(CompanyDocument.from(company));
        return globalDocument;
    }
}
