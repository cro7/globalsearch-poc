package de.xchange.globalsearch.documents;

import de.xchange.globalsearch.interfaces.amqp.dto.Company;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
@AllArgsConstructor
public class CompanyDocument implements Identifiable {

    private String id;
    private String companyName;
    private String companyAcronym;
    private String companyType;
    private String city;
    private String country;
    private Set<String> networkNames;
    private String contactName;
    private String contactEmail;
    private String publicProfileDomainHandle;

    public static CompanyDocument from(Company company) {
        return new CompanyDocument(company.getId(), company.getCompanyName(), company.getCompanyAcronym(), company.getCompanyType(),
            company.getCity(),
            company.getCountry(), company.getNetworkNames(), company.getContactName(), company.getContactEmail(),
            company.getPublicProfileDomainHandle());
    }

}
