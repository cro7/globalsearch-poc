package de.xchange.globalsearch.documents;

import de.xchange.globalsearch.interfaces.amqp.dto.CompanyName;
import de.xchange.globalsearch.interfaces.amqp.dto.EquipmentType;
import de.xchange.globalsearch.interfaces.amqp.dto.Location;
import de.xchange.globalsearch.interfaces.amqp.dto.ReleasedContainer;
import de.xchange.globalsearch.interfaces.amqp.dto.TradingDeal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Getter
@Setter
@ToString(callSuper = true)
@AllArgsConstructor
public class TradingDealDocument implements Identifiable {

    private String id;
    @Field(type = FieldType.Object)
    private Location location;
    @Field(type = FieldType.Object)
    private EquipmentType equipmentType;
    @Field(type = FieldType.Object)
    private CompanyName sellerCompany;
    @Field(type = FieldType.Object)
    private CompanyName buyerCompany;
    @Field(type = FieldType.Nested, includeInParent = true)
    private List<ReleasedContainer> releasedContainers;
    private String conditionName;
    private String quantity;
    private String price;

    public static TradingDealDocument from(TradingDeal tradingDeal) {
        return new TradingDealDocument(tradingDeal.getId(), tradingDeal.getLocation(), tradingDeal.getEquipmentType(), tradingDeal.getSellerCompany(),
            tradingDeal.getBuyerCompany(), tradingDeal.getReleasedContainers(), tradingDeal.getConditionName(), tradingDeal.getQuantity(),
            tradingDeal.getPrice());
    }
}
