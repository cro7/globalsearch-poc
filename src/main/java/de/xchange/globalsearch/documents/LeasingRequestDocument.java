package de.xchange.globalsearch.documents;

import de.xchange.globalsearch.interfaces.amqp.dto.CompanyName;
import de.xchange.globalsearch.interfaces.amqp.dto.EquipmentType;
import de.xchange.globalsearch.interfaces.amqp.dto.LeasingRequest;
import de.xchange.globalsearch.interfaces.amqp.dto.Location;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Getter
@Setter
@ToString(callSuper = true)
@AllArgsConstructor
public class LeasingRequestDocument implements Identifiable {

    private String id;
    private String requestStatus;
    @Field(type = FieldType.Object)
    private CompanyName requesterCompany;
    @Field(type = FieldType.Object)
    private CompanyName addresseeCompany;
    @Field(type = FieldType.Object)
    private EquipmentType equipmentType;
    private String unitCondition;
    @Field(type = FieldType.Nested, includeInParent = true)
    private List<Location> dropOffLocations;
    @Field(type = FieldType.Nested, includeInParent = true)
    private List<Location> pickUpLocations;

    public static LeasingRequestDocument from(LeasingRequest leasingRequest) {
        return new LeasingRequestDocument(leasingRequest.getId(), leasingRequest.getRequestStatus(), leasingRequest.getRequesterCompany(),
            leasingRequest.getAddresseeCompany(), leasingRequest.getEquipmentType(), leasingRequest.getUnitCondition(),
            leasingRequest.getDropOffLocations(), leasingRequest.getPickUpLocations());
    }

}
