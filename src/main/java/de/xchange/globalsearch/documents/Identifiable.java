package de.xchange.globalsearch.documents;

public interface Identifiable {

    String getId();

}
