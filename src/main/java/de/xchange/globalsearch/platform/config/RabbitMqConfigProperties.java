package de.xchange.globalsearch.platform.config;

import java.time.Duration;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "application.rabbitmq")
@Getter
@Setter
public class RabbitMqConfigProperties {

    private int batchSize = 1;
    private Duration receiveTimeout = Duration.ofSeconds(1L);
}
