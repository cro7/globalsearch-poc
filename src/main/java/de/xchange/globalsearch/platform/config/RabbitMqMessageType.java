package de.xchange.globalsearch.platform.config;

public enum RabbitMqMessageType {
    COMPANY_COMPANY,
    TRADING_DEAL,
    LEASING_REQUEST;

    public static final String ACTION_KEY = "action";
}
