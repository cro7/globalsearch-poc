package de.xchange.globalsearch.platform.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "application.elasticsearch")
@Getter
@Setter
public class ElasticsearchConfigProperties {

    private String host;
    private int port;
    private String username;
    private String password;
    private boolean usingSsl;
}
