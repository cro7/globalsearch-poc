package de.xchange.globalsearch.platform.config;

import java.time.Duration;
import javax.net.ssl.SSLContext;
import lombok.SneakyThrows;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.ClientConfiguration.MaybeSecureClientConfigurationBuilder;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

@Configuration
public class ElasticsearchConfig extends AbstractElasticsearchConfiguration {

    private final ElasticsearchConfigProperties properties;

    public ElasticsearchConfig(ElasticsearchConfigProperties properties) {
        this.properties = properties;
    }

    @SneakyThrows
    @Override
    @Bean
    public RestHighLevelClient elasticsearchClient() {
        MaybeSecureClientConfigurationBuilder configBuilder = ClientConfiguration.builder()
            .connectedTo("%s:%s".formatted(properties.getHost(), properties.getPort()));
        if (properties.isUsingSsl()) {
            configBuilder.usingSsl(SSLContext.getDefault(), NoopHostnameVerifier.INSTANCE);
        }
        if (properties.getUsername() != null && !properties.getUsername().isEmpty()) {
            configBuilder.withBasicAuth(properties.getUsername(), properties.getPassword());
        }
        configBuilder.withConnectTimeout(Duration.ofSeconds(10L));
        configBuilder.withSocketTimeout(Duration.ofSeconds(10L));
        return RestClients.create(configBuilder.build()).rest();
    }
}
