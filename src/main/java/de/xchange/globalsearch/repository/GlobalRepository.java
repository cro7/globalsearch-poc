package de.xchange.globalsearch.repository;

import de.xchange.globalsearch.documents.GlobalDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface GlobalRepository extends ElasticsearchRepository<GlobalDocument, String> {

}
