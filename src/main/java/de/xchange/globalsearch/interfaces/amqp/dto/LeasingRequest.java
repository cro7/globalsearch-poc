package de.xchange.globalsearch.interfaces.amqp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LeasingRequest {

    private String id;
    @JsonProperty("request_status")
    private String requestStatus;
    @JsonProperty("requester_company")
    private CompanyName requesterCompany;
    @JsonProperty("addressee_company")
    private CompanyName addresseeCompany;
    @JsonProperty("unit_condition")
    private String unitCondition;
    @JsonProperty("equipment_type")
    private EquipmentType equipmentType;
    @JsonProperty("dropoff_locations")
    private List<Location> dropOffLocations;
    @JsonProperty("pickup_locations")
    private List<Location> pickUpLocations;

}
