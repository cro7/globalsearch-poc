package de.xchange.globalsearch.interfaces.amqp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReleasedContainer {

    @JsonProperty("release_reference")
    private String releaseReference;
    @JsonProperty("container_number")
    private String containerNumber;
}
