package de.xchange.globalsearch.interfaces.amqp.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CompanyName {

    private String id;
    private String name;
}
