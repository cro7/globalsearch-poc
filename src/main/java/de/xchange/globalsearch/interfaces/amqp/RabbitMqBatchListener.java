package de.xchange.globalsearch.interfaces.amqp;

import static de.xchange.globalsearch.platform.config.RabbitMqConfig.QUEUE_NAME_INPUT;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.xchange.globalsearch.interfaces.amqp.dto.Company;
import de.xchange.globalsearch.interfaces.amqp.dto.LeasingRequest;
import de.xchange.globalsearch.interfaces.amqp.dto.TradingDeal;
import de.xchange.globalsearch.platform.config.InvalidPayloadException;
import de.xchange.globalsearch.platform.config.RabbitMqMessageType;
import de.xchange.globalsearch.services.IngestionService;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitMqBatchListener {

    private final IngestionService ingestionService;
    private final ObjectMapper objectMapper;
    private final LocalValidatorFactoryBean validator;

    @RabbitListener(queues = QUEUE_NAME_INPUT, containerFactory = "simpleRabbitListenerContainerFactory")
    public void handleMessages(List<Message> rabbitMessages) {
        log.debug("received rabbitMessages: {}", rabbitMessages.size());
        Map<RabbitMqMessageType, List<Message>> messagesMappedByType = rabbitMessages.stream()
            .collect(groupingBy(message -> RabbitMqMessageType.valueOf(message.getMessageProperties().getHeader(RabbitMqMessageType.ACTION_KEY))));
        for (Entry<RabbitMqMessageType, List<Message>> entry : messagesMappedByType.entrySet()) {
            switch (entry.getKey()) {
                case COMPANY_COMPANY -> {
                    List<Company> companies = entry.getValue().stream().map(message -> map(message.getBody(), Company.class))
                        .collect(toList());
                    log.debug("received companies: {}", companies.size());
                    ingestionService.upsertCompanys(companies);
                }
                case LEASING_REQUEST -> {
                    List<LeasingRequest> leasingRequests = entry.getValue().stream().map(message -> map(message.getBody(), LeasingRequest.class))
                        .collect(toList());
                    log.debug("received leasingRequests: {}", leasingRequests.size());
                    ingestionService.upsertLeasingRequests(leasingRequests);
                }
                case TRADING_DEAL -> {
                    List<TradingDeal> tradingDeals = entry.getValue().stream().map(message -> map(message.getBody(), TradingDeal.class))
                        .collect(toList());
                    log.debug("received tradingDeals: {}", tradingDeals.size());
                    ingestionService.upsertTradingDeals(tradingDeals);
                }
                default -> log.error("cannot handle {}", entry.getKey());
            }
        }
    }

    public <T> T map(byte[] src, Class<T> targetType) {
        try {
            T t = objectMapper.readValue(src, targetType);
            Set<ConstraintViolation<T>> violations = validator.validate(t);
            if (!violations.isEmpty()) {
                throw new ConstraintViolationException(violations);
            }
            return t;
        } catch (IOException | ConstraintViolationException ex) {
            log.error(ex.getMessage());
            throw new InvalidPayloadException(ex.getMessage());
        }
    }
}