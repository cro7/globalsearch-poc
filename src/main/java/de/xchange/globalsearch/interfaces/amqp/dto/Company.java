package de.xchange.globalsearch.interfaces.amqp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Company {

    private String id;
    @JsonProperty("company_name")
    private String companyName;
    @JsonProperty("company_acronym")
    private String companyAcronym;
    @JsonProperty("company_type")
    private String companyType;
    private String city;
    private String country;
    @JsonProperty("network_names")
    private Set<String> networkNames;
    @JsonProperty("contact_name")
    private String contactName;
    @JsonProperty("contact_email")
    private String contactEmail;
    @JsonProperty("public_profile_domain_handle")
    private String publicProfileDomainHandle;
}
