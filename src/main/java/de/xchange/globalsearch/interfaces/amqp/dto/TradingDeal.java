package de.xchange.globalsearch.interfaces.amqp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TradingDeal {

    private String id;
    private Location location;
    @JsonProperty("condition_name")
    private String conditionName;
    @JsonProperty("equipment_type")
    private EquipmentType equipmentType;
    @JsonProperty("seller_company")
    private CompanyName sellerCompany;
    @JsonProperty("buyer_company")
    private CompanyName buyerCompany;
    @JsonProperty("released_containers")
    private List<ReleasedContainer> releasedContainers;
    private String quantity;
    private String price;

}
