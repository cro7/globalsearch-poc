package de.xchange.globalsearch.interfaces.amqp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EquipmentType {

    private String name;
    @JsonProperty("short_name")
    private String shortName;
    @JsonProperty("long_name")
    private String longName;
}
