package de.xchange.globalsearch.interfaces.amqp.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Location {

    private String id;
    private String name;
    private String unlocode;
    private String region;
    private String country;
}
