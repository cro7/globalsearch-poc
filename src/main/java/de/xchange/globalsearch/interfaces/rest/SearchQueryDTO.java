package de.xchange.globalsearch.interfaces.rest;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class SearchQueryDTO {

    String searchString;

    int pageSize;
    int pageNum;

}
