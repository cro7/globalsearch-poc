package de.xchange.globalsearch.interfaces.rest;

import de.xchange.globalsearch.documents.GlobalDocument;
import de.xchange.globalsearch.services.SearchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RequestMapping("/api/search")
@RestController
@Slf4j
public class SearchController {

    private final SearchService searchService;

    @PostMapping("/global")
    public SearchHits<GlobalDocument> searchGlobal(@RequestBody SearchQueryDTO searchQuery) {
        log.debug("got searchQuery: {}", searchQuery);
        return searchService.searchGlobal(searchQuery);
    }

}
